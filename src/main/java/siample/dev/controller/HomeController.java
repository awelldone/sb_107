package siample.dev.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import siample.dev.service.SpyService;

@RestController
public class HomeController {
	
	@Autowired
	private SpyService spicy;
	
	@Value("$(HomeController.msg)")
	private String message;
	
	@RequestMapping("/")
	public String index() {
		return spicy.iSaySg() + message;
	}
}
