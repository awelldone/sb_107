package siample.dev.service;

import org.springframework.stereotype.Service;

@Service("spying")
public class SpyService {

	public String iSaySg() {
		return "sb_107: @Value related to app.props";
	}
}
